# Base Image, Python Interpreter
FROM python:3.7.2-stretch AS basepython
RUN python -m pip install --upgrade pip setuptools

# Application
FROM basepython AS application
RUN adduser --uid 999 appuser --quiet
RUN mkdir /data
RUN chown appuser:appuser /data
USER root
COPY . /project
RUN python -m pip install -e /project/src
USER appuser

# Jupyter Notebook running on top of the application
FROM application AS jupyter
USER root
RUN python -m pip install -r /project/src/requirements_jupyter.txt
USER appuser
RUN jupyter nbextension enable --py widgetsnbextension
RUN jupyter nbextensions_configurator enable
ENTRYPOINT jupyter

