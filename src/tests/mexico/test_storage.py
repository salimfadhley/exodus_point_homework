import datetime
import unittest
from dataclasses import dataclass

from mexico.storage import open_pickle_share




class TestStorage(unittest.TestCase):

    def setUp(self):
        self.s = open_pickle_share("testing")

    def tearDown(self):
        self.s.clear()

    def test_store_a_key(self):
        dt:datetime.date = datetime.date.today()
        series_name = "XXY"
        series_value = {"Hello":True}
        self.s.store_object(date=dt, key=series_name, value=series_value)
        result = self.s.get_object(date=dt, key=series_name)
        self.assertEqual(series_value, result)

if __name__ == "__main__":
    unittest.main()
