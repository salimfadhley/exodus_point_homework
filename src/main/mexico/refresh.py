import logging
import sys
from typing import Sequence
import dateutil.parser
from mexico.config import get_series_names, get_bmx_token
from mexico.date_range import get_refresh_date_range
from mexico.sie import get_series_data
from mexico.storage import DataThing, DataPoint,  open_default_pickle_share

log = logging.getLogger(__name__)



def store_data(datos:Sequence[DataThing], idSerie:str, **_):
    storage = open_default_pickle_share()
    for data_point in datos:
        observation_date = dateutil.parser.parse(data_point['fecha']).date()
        value = float(data_point["dato"])
        dp:DataPoint = DataPoint(idSerie, value, observation_date)
        storage.store_object(observation_date, idSerie, dp)


def refresh()->int:
    log.info("Refreshing data.")
    series_names:Sequence[str] = get_series_names()
    date_range = get_refresh_date_range()

    log.info(f"Refreshing {series_names} for {date_range}")

    series_data = get_series_data(get_series_names(), token=get_bmx_token())

    for series in series_data:
        store_data(**series)

    return 0

if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger("").setLevel(logging.INFO)
    sys.exit(refresh())
