import dataclasses
import datetime
import functools
import logging
import os
import pprint

import pandas
from dataclasses import dataclass
from typing import Union, Mapping

import pickleshare
from mexico.date_range import DateRange, get_refresh_date_range

from mexico.config import get_pickleshare_directory, get_series_names

DataThing = Mapping[str,Union["DataThing",str,float]]

log = logging.getLogger(__name__)

@dataclass
class DataPoint:
    series: str
    value: float
    date: datetime.date

@dataclass
class StorageThing:
    pdb: pickleshare.PickleShareDB

    def clear(self):
        self.pdb.clear()


    @classmethod
    def get_key(cls, date:datetime.date, key:str):
        return f"{key}/{date.isoformat()}"

    def keys(self):
        return self.pdb.keys()

    def store_object(self, date:datetime.date, key:str, value:DataPoint)->None:
        storage_key = self.get_key(date, key)
        log.info("Storing %s->%r", storage_key, value)
        self.pdb[storage_key] = value

    def get_object(self, date:datetime.date, key:str)->DataPoint:
        return self.pdb[self.get_key(date, key)]

    def load_range(self, series_name:str, start_date: datetime.date, end_date:datetime.date)->pandas.DataFrame:
        dr:DateRange = DateRange(start_date, end_date)
        values = []
        for d in dr.range_inclusive():
            try:
                data:DataPoint = self.get_object(d, series_name)
                values.append(dataclasses.asdict(data))
            except KeyError:
                log.info("No value for %r", d)

        return pandas.DataFrame(values)


def open_pickle_share(namespace:str)->StorageThing:
    pickle_dir = os.path.join(get_pickleshare_directory(), namespace)
    pdb = pickleshare.PickleShareDB(pickle_dir)
    return StorageThing(pdb=pdb)

def open_default_pickle_share()->StorageThing:
    return open_pickle_share("default")


def load_series(series_name:str, start_date:datetime.date, end_date:datetime.date):
    ps = open_default_pickle_share()
    return ps.load_range(series_name, start_date, end_date)
