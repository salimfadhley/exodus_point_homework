import os
from typing import Sequence

def get_pickleshare_directory()->str:
    return os.environ.get("PICKLESHARE_ROOT")


def get_bmx_token()->str:
    return os.environ.get("BMX_TOKEN")

def get_series_names()->Sequence[str]:
    return os.environ.get("SERIES_NAMES").split(",")

def get_refresh_days()->int:
    return int(os.environ.get("REFRESH_DAYS"))