import datetime
import logging
from dataclasses import dataclass
from typing import Sequence

import requests
import pprint

from mexico.config import get_bmx_token, get_series_names
from bunch import Bunch

log = logging.getLogger(__name__)


class SIEError(RuntimeError):
    """API error has occurred"""


def get_series_data(series_names:Sequence[str], token:str, end_date:datetime.date=None, start_date:datetime.date=None)->Bunch:
    end_date = end_date or datetime.date.today()
    start_date = start_date or end_date - datetime.timedelta(days=90)
    series_string = ",".join(series_names)
    url = f"https://www.banxico.org.mx/SieAPIRest/service/v1/series/{series_string}/datos/{start_date.isoformat()}/{end_date.isoformat()}"
    log.info("URL: %s", url)
    r:requests.Response = requests.get(
        url,
        headers={
            "Bmx-Token": token
        }

    )
    if r.status_code != 200:
        raise SIEError(r.text)
    return Bunch(r.json())["bmx"]["series"]


if __name__ == "__main__":
    pprint.pprint(get_series_data(get_series_names(), token=get_bmx_token()))