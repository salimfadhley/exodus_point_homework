import datetime
from dataclasses import dataclass
from typing import Iterator

from mexico.config import get_refresh_days


@dataclass
class DateRange:
    start_date: datetime.date
    end_date: datetime.date

    def range_inclusive(self)->Iterator[datetime.date]:
        assert self.end_date >= self.start_date, "End date cannot be before start date."
        d = self.start_date
        while d <= self.end_date:
            yield d
            d = d + datetime.timedelta(days=1)


def get_refresh_date_range() -> DateRange:
    days_back:int = get_refresh_days()
    today = datetime.date.today()
    start_date = today - datetime.timedelta(days=days_back)
    return DateRange(start_date, today)
