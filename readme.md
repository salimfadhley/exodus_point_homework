Exodus Point Homework - Salim Fadhley
==

### Contact information

* salimfadhley@gmail.com
* 07973 710 574

### Running the project:

#### Requirements:

* Docker-ce with Docker-compose
* A UNIX / Mac system
* Chrome web-browser

#### How to run:

The project is self-contained
```bash
$ git pull git@gitlab.com:salimfadhley/exodus_point_homework.git
$ cd exodus_point_homework
$ docker-compose up
```
If this is your first run, docker will download all of the components
required to make this project run. It will start a number of containers,
one of which will run Jupyter notebook.

If the system is building correctly you should see something like this truncated output:

```bash
sal@gruntyman:~/PycharmProjects/exoduspoint_homework$ docker-compose up --build
Building python
Step 1/10 : FROM python:3.7.2-stretch AS basepython
 ---> 2fbd95050b66
Step 2/10 : RUN python -m pip install --upgrade pip setuptools
 ---> Using cache
 ---> 6c1e5e7f0481

Step 3/10 : FROM basepython AS application
 ---> 6c1e5e7f0481
Step 4/10 : RUN adduser --uid 999 appuser --quiet
 ---> Using cache
 ---> f509b29e916f
Step 5/10 : RUN mkdir /data
 ---> Using cache
 ---> 30e67bb553df
Step 6/10 : RUN chown appuser:appuser /data
 ---> Using cache
 ---> 17dfa6485289
Step 7/10 : USER root
```

Once all of the images have been built Docker-Compose will start up two conainers.

* The first "refresh" container will connect to Banca Di Mexico and fetch data.
* The second "jupyter" container will start up a Jupyter notebook server.

Take note of the activation key for Jupyter notebook on the docker log, you
will need this to activate the notebook-server which is running on [http://localhost:8282](http://localhost:8282).

```
jupyter_1  |     To access the notebook, open this file in a browser:
jupyter_1  |         file:///home/appuser/.local/share/jupyter/runtime/nbserver-1-open.html
jupyter_1  |     Or copy and paste one of these URLs:
jupyter_1  |         http://(1a2355efde4e or 127.0.0.1):8282/?token=e91e57f5fec58a3a9df7048a64a2ae90e6fd69575797c054
```

In this case, the token is ```e91e57f5fec58a3a9df7048a64a2ae90e6fd69575797c054```

Docker compose automatically loads in however many days worth of data is configured
in the docker-compose yaml file, so there are no extra steps.

#### How to add / change series selection

Edit the `docker-compose.yaml` file - you will see that the defaults such
as how many days to load and the names of the series are in a structure
near the top.

#### Exercise details:

On this page you will find historic outstanding debt from the Banco De Mexico

* http://www.anterior.banxico.org.mx/valores/LeePeriodoSectorizacionValores.faces?BMXC_claseIns=GUB&BMXC_lang=en_MX

SIE data API:

* https://www.banxico.org.mx/SieAPIRest/service/v1/?locale=en

Data is presented for different types of bonds. Write a program, in the choice of your language, to build a database to store this data for 3 Months. Build a front end to display the holdings by Sector, and Bond Type.  

You are free to choose the database and front end technology. For example SQLite, Flask and D3.js or Plotly dash may be an option.   

One other thing to keep in mind is that Banco De Mexico offer a REST service from which this data may be available, further exploration is required.
